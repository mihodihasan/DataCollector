package me.mihodihasan.datacollector;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    private static final int RC_SIGN_IN = 1;
    private static final String MY_PREFS_NAME = "mihodi_hasan_lushan";
    final String TAG = "ERROR_TAG";
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    FirebaseUser firebaseUser;
    private MyService myService;
    private ServiceConnection serviceConnection;
    private Notification.Builder notificationBuilder;
    private NotificationManager notificationManager;
    private int NOTIFICATION_ID = 55;
    static final int importance = NotificationManagerCompat.IMPORTANCE_HIGH;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        askPermission();
        serviceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName className, IBinder service) {

                myService = ((MyService.MyBinder) service).getService();
                Log.d(TAG, "onServiceConnected: " + myService);
                addNotification();
                myService.startForeground(NOTIFICATION_ID, notificationBuilder.build());
            }

            public void onServiceDisconnected(ComponentName className) {
                Log.d(TAG, "onServiceDisconnected: ");
                myService = null;
            }
        };

        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                firebaseUser = firebaseAuth.getCurrentUser();
                if (firebaseUser != null) {
                    //Sign in
                    Toast.makeText(MainActivity.this, firebaseUser.getDisplayName(), Toast.LENGTH_SHORT).show();
                    FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
                    DatabaseReference root = firebaseDatabase.getReference();
                    Log.d(TAG, "onAuthStateChanged: pushing");
                } else {
                    //Sign Out
                    startActivityForResult(
                            AuthUI.getInstance()
                                    .createSignInIntentBuilder()
                                    .setIsSmartLockEnabled(false)
                                    .setAvailableProviders(
                                            Arrays.asList(
                                                    new AuthUI.IdpConfig.GoogleBuilder().build()
                                            )
                                    )
                                    .build(), RC_SIGN_IN);
                }
            }
        };
        firebaseAuth.addAuthStateListener(authStateListener);


    }

    void checkOreoNotificationSupport(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            CharSequence name = "Name";
            String description = "Description";
            NotificationChannel channel = new NotificationChannel("mihodihasan", name, importance);
            channel.setDescription(description);
            channel.setLightColor(Color.CYAN);
            // Register the channel with the system
            notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
            notificationBuilder.setChannelId("mihodihasan");
        }
    }

    private void addNotification() {
        // create the notification
        notificationBuilder = new Notification.Builder(this)
                .setContentTitle("Name")
                .setContentText("Description")
                .setSmallIcon(R.mipmap.ic_launcher);

        // create the pending intent and add to the notification
        Intent intent = new Intent(this, MyService.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        notificationBuilder.setContentIntent(pendingIntent);
        // send the notification
        notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        checkOreoNotificationSupport();
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                startService(new Intent(this, MyService.class));
                Log.d(TAG, "onRequestPermissionsResult: " + myService);
//                if (myService!=null){
//                    bindService(serviceConnection);
//                }
//                Toast.makeText(this, "Your Gps Data Started Sending To Lushan", Toast.LENGTH_SHORT).show();
            } else {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public boolean askPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
                if (!prefs.getBoolean("askingPermissionFirst", true)) {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.
                            ACCESS_FINE_LOCATION)) {
//                    Explain
                        final AlertDialog.Builder builder = new AlertDialog.Builder(this)
                                .setTitle("Need Permission!")
                                .setMessage("App needs permission to write in your SD card so that app can download " +
                                        "images into your storage and you can read the posts offline. \n If you want " +
                                        "this offline feature, you have to give permission!\n Wanna permit us??")
                                .setPositiveButton("YES!", new DialogInterface.OnClickListener() {
                                    @RequiresApi(api = Build.VERSION_CODES.M)
                                    public void onClick(DialogInterface dialog, int which) {
                                        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                                    }
                                })
                                .setNegativeButton("NO!", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                        builder.setIcon(android.R.drawable.ic_dialog_alert)
                                .setCancelable(false)
                                .show();
                    } else {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(this)
                                .setTitle("Permission Denied!")
                                .setMessage("Sir, Previously You Denied to Access Us To Write On Your Storage, " +
                                        "We cant ask you for this, please go to settings and permit us manually if you " +
                                        "need this feature, We only Can show you the pathway!\nJust Tap Settings and then " +
                                        "go to permissions and tap on the togglebar")
                                .setPositiveButton("YES!", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent i = new Intent();
                                        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        i.addCategory(Intent.CATEGORY_DEFAULT);
                                        i.setData(Uri.parse("package:" + MainActivity.this.getPackageName()));
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                        startActivity(i);
                                    }
                                })
                                .setNegativeButton("NO!", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                        builder.setIcon(android.R.drawable.ic_dialog_alert)
                                .setCancelable(false)
                                .show();
                    }
                    return false;
                } else {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putBoolean("askingPermissionFirst", false);
                    editor.commit();
                }
            }
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Signed in!", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Sign in cancelled!", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sign_out_menu:
                AuthUI.getInstance().signOut(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void startMyService(View view) {
        Intent intent = new Intent(this, MyService.class);
        if (askPermission()) {
            bindService(intent, serviceConnection, BIND_AUTO_CREATE);
            Toast.makeText(this, "Your Gps Data Started Sending To Lushan", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "You Didn't Approve Permission!", Toast.LENGTH_SHORT).show();
        }

    }

    public void stopMyService(View view) {
        unbindService(serviceConnection);
    }
}
